package AssignSpringBoot.Assignment_SpringbootForInternship.Repository;

import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, String> {

}
