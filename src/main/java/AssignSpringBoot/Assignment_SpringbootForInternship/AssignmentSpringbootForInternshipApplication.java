package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Mapper.UserMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AssignmentSpringbootForInternshipApplication {

    @Bean
    public UserMapper modelMapper() {
        return new UserMapper();
    }

    public static void main(String[] args) {
        SpringApplication.run(AssignmentSpringbootForInternshipApplication.class, args);
    }

}
