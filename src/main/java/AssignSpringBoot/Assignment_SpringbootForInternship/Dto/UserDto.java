package AssignSpringBoot.Assignment_SpringbootForInternship.Dto;

import lombok.Data;

@Data
public class UserDto {
    private String id;
    private String team;
    private String surname;
    private String lastname;
    private String nickname;
    private String teamposition;
    private String email;
    private String phone;
}
