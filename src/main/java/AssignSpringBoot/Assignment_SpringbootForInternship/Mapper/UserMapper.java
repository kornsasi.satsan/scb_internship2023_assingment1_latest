package AssignSpringBoot.Assignment_SpringbootForInternship.Mapper;

import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public static UserDto toDto(UserEntity userEntity) {
        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setSurname(userEntity.getSurname());
        userDto.setLastname(userEntity.getLastname());
        userDto.setEmail(userEntity.getEmail());
        userDto.setPhone(userEntity.getPhone());
        userDto.setTeam(userEntity.getTeam());
        userDto.setNickname(userEntity.getNickname());
        userDto.setTeamposition(userEntity.getTeamposition());
        return userDto;
    }

    public static UserEntity toEntity(UserDto userDto) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userDto.getId());
        userEntity.setSurname(userDto.getSurname());
        userEntity.setLastname(userDto.getLastname());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setPhone(userDto.getPhone());
        userEntity.setTeam(userDto.getTeam());
        userEntity.setNickname(userDto.getNickname());
        userEntity.setTeamposition(userDto.getTeamposition());
        return userEntity;
    }

}
