package AssignSpringBoot.Assignment_SpringbootForInternship.Service;

import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Mapper.UserMapper;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import AssignSpringBoot.Assignment_SpringbootForInternship.Repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
public class UserService implements UserServiceInterface {
    @Autowired
    private UserRepository userRepository;
//    @Autowired
//    private UserMapper userMapper;
//    public UserService(UserRepository userRepository, UserMapper userMapper){
//        this.userRepository = userRepository;
//        this.userMapper = userMapper;
//    }

    @Override
    public UserDto getUserByID(String id) {
        Optional<UserEntity> userWasFound = userRepository.findById(id);
        if (userWasFound.isPresent()) {
            return UserMapper.toDto(userWasFound.get());
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User id: " + id + " Not Found");
    }

    @Override
    public UserEntity createNewUser(UserEntity newUser) {
        if (StringUtils.isEmpty(newUser.getSurname()) ||
                StringUtils.isEmpty(newUser.getLastname()) ||
                StringUtils.isEmpty(newUser.getEmail()) ||
                StringUtils.isEmpty(newUser.getPhone())
        ) {
            log.error("Incomplete user data when creating new user");
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        userRepository.save(newUser);
        log.info("User created:" + newUser.getSurname() + " " + newUser.getLastname());
        return newUser;
    }

    @Override
    public List<UserDto> getAllUser() {
        List<UserEntity> listUser = userRepository.findAll();
        if (listUser.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        List<UserDto> listUserDto = new ArrayList<UserDto>();
        for (UserEntity e : listUser) {
            UserDto userDto = UserMapper.toDto(e);
            listUserDto.add(userDto);
        }
        return listUserDto;
    }

    @Override
    public UserDto updateUserData(String id, UserDto editUserDto) {
        Optional<UserEntity> findUser = userRepository.findById(id);
        if (!findUser.isPresent()) {
            log.error("Not found user when updating");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        UserEntity userWasFound = findUser.get();
        userWasFound.setSurname(editUserDto.getSurname());
        userWasFound.setLastname(editUserDto.getLastname());
        userWasFound.setEmail(editUserDto.getEmail());
        userWasFound.setPhone(editUserDto.getPhone());
        userWasFound.setTeam(editUserDto.getTeam());
        userWasFound.setNickname(editUserDto.getNickname());
        userWasFound.setTeamposition(editUserDto.getTeamposition());
        userRepository.save(userWasFound);
        log.info("User id updated: " + id);
        return editUserDto;
    }

    @Override
    public UserDto updatePartialUser(String id, UserDto partialEditUser) throws IllegalAccessException {
        Optional<UserEntity> findUser = userRepository.findById(id);
        if (!findUser.isPresent()) {
            log.error("Not found user when updating");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        UserEntity userWasFound = findUser.get();
        UserEntity partialEditUserEntity = UserMapper.toEntity(partialEditUser);
        Class<UserEntity> userClass = UserEntity.class;
        Field[] fields = userClass.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(partialEditUserEntity);
            if (value == null) {
            } else {
                field.set(userWasFound, value);
            }
        }
        userRepository.save(userWasFound);
        log.info("User id updated: " + id);
        UserDto updatepartialUserDto = UserMapper.toDto(userWasFound);
        return updatepartialUserDto;
    }

    @Override
    public ResponseEntity deleteUser(String id) {
        Optional<UserEntity> findUser = userRepository.findById(id);
        if (!findUser.isPresent()) {
            log.error("Not found user when deleting");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        userRepository.deleteById(id);
        log.info("User id deleted: " + id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
