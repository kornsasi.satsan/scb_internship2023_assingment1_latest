package AssignSpringBoot.Assignment_SpringbootForInternship.Service;

import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserServiceInterface {
    UserDto getUserByID(String id);

    UserEntity createNewUser(UserEntity newUser);

    List<UserDto> getAllUser();

    UserDto updateUserData(String id, UserDto editUserEntity);

    UserDto updatePartialUser(String id, UserDto partialEditUser) throws IllegalAccessException;

    ResponseEntity deleteUser(String id);


}
