package AssignSpringBoot.Assignment_SpringbootForInternship.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    private final Logger log = LogManager.getFormatterLogger(Producer.class);
    private final String TOPIC = "TopicMessage";
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public String sendMessage(String message) {
        try {
            log.info("Produce Topic name: %s - Text message for send: %s", TOPIC, message);
            this.kafkaTemplate.send(TOPIC, message);
            return "message is sent.";
        } catch (Exception e) {
            e.printStackTrace();
            return "message is not sent.";
        }
    }
}
