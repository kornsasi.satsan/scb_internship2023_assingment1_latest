package AssignSpringBoot.Assignment_SpringbootForInternship.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {
    private final Logger log = LogManager.getFormatterLogger(Consumer.class);

    @KafkaListener(topics = "TopicMessage", groupId = "Group")
    public String getMassage(String message) throws IOException {
        log.info("Received message from producer: %s", message);
        return "Message is received from producer";
    }
}
