package AssignSpringBoot.Assignment_SpringbootForInternship.Controller;

import AssignSpringBoot.Assignment_SpringbootForInternship.Service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/producer")
public class ProducerController {

    @Autowired
    private Producer producer;

    @GetMapping
    public ResponseEntity<String> sentMessage(@RequestParam("message") String message) {
        producer.sendMessage(message);
        return ResponseEntity.ok()
                .body("Producer sent message");
    }
}
