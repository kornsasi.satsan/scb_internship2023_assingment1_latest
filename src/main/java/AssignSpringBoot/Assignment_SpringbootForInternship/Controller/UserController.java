package AssignSpringBoot.Assignment_SpringbootForInternship.Controller;

import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Mapper.UserMapper;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import AssignSpringBoot.Assignment_SpringbootForInternship.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

//    private final UserService userService;
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }

    @GetMapping("/hello")
    public String Hello() {
        return "Hello Backend Team";
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable(name = "id") String id) {
        UserDto result = userService.getUserByID(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(result);
    }

    @PostMapping
    public ResponseEntity<UserDto> createNewUser(@RequestBody UserDto userDto) {
        UserEntity newUserEntity = UserMapper.toEntity(userDto);
        userService.createNewUser(newUserEntity);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userDto);
    }

    @GetMapping
    public List<UserDto> getAllUser() {
        return userService.getAllUser();
    }

    @PutMapping("/{id}")
    public UserDto updateUser(@PathVariable(name = "id") String id,
                              @RequestBody UserDto userDto) {
        return userService.updateUserData(id, userDto);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<UserDto> updatePartialUser(@PathVariable(name = "id") String id,
                                                     @RequestBody UserDto updatePartialData) {
        try {
            userService.updatePartialUser(id, updatePartialData);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(updatePartialData);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .body(updatePartialData);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable(name = "id") String id) {
        return userService.deleteUser(id);
    }
}
