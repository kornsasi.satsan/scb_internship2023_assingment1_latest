package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Controller.ProducerController;
import AssignSpringBoot.Assignment_SpringbootForInternship.Service.Producer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProducerController.class)
@ExtendWith(MockitoExtension.class)
public class ProducerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private Producer producerService;

    @InjectMocks
    private ProducerController producerController;

    @Test
    public void sentMessageSuccess() throws Exception {
        String url = "/api/v1/producer?message='messagesFromRequest'";
        String message = "message is sent.";
        doReturn("Pass")
                .when(producerService)
                .sendMessage(message);
        ResultActions result = mvc.perform(get(url));
        result.andExpect(status().isOk());
//        var actual = producerController.sentMessage(message);
//        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }
}
