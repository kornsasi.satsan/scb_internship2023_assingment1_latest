package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import AssignSpringBoot.Assignment_SpringbootForInternship.Repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserRepositoryTest {
    @Mock
    UserRepository userRepository;
    @Test
    public void repositoryFindByIdTest() {
        String id = "userId";
        UserEntity user = new UserEntity();
        user.setId(id);
        when(userRepository.findById(id))
                .thenReturn(Optional.of(user));
        Optional<UserEntity> result = userRepository.findById(id);
        assertEquals(user.getId(), result.get().getId());
    }

    @Test
    public void repositoryFindAllUserTest() {
        List<UserEntity> listUserEntity = new ArrayList<UserEntity>();
        UserEntity userFound = new UserEntity();
        userFound.setSurname("Atom");
        listUserEntity.add(userFound);
        when(userRepository.findAll()).thenReturn(listUserEntity);
        List<UserEntity> result = userRepository.findAll();
        assertEquals(result.get(0).getSurname(), listUserEntity.get(0).getSurname());
    }

    @Test
    public void repositorySaveUserTest() {
        UserEntity user = new UserEntity();
        user.setSurname("Korn");
        user.setLastname("lastName");
        user.setPhone("0811111111");
        user.setEmail("user@example.com");
        when(userRepository.save(user)).thenReturn(user);
        UserEntity result = userRepository.save(user);
        assertEquals(result.getSurname(), user.getSurname());
    }

}
