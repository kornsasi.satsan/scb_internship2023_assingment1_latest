package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Mapper.UserMapper;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import AssignSpringBoot.Assignment_SpringbootForInternship.Repository.UserRepository;
import AssignSpringBoot.Assignment_SpringbootForInternship.Service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @Autowired
    UserMapper mapper;
    @InjectMocks
    UserService userService;

    public UserServiceTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void successGetUserByIDTest(){
        String userId = "userId";
        UserEntity  newUserEntity = new UserEntity();
        newUserEntity.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(newUserEntity));
        UserDto result = userService.getUserByID(userId);
        assertEquals(newUserEntity.getId(), result.getId());
    }

    @Test
    public void notFoundForGetUserByIDTest(){
        String userId = "userId";
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> {
            userService.getUserByID(userId);
        });
    }

    @Test
    public void successCreateNewUserTest(){
        UserEntity  newUserEntity = new UserEntity();
        newUserEntity.setSurname("Korn");
        newUserEntity.setLastname("lastName");
        newUserEntity.setPhone("0811111111");
        newUserEntity.setEmail("user@example.com");
        when(userRepository.save(newUserEntity)).thenReturn(newUserEntity);
        UserEntity result = userService.createNewUser(newUserEntity);
        assertEquals(newUserEntity.getSurname(), result.getSurname());
    }

    @Test
    public void haveNullEntityInCreateNewUserTest(){
        UserEntity nullUserEntity = new UserEntity();
        assertThrows(ResponseStatusException.class, () -> {
            userService.createNewUser(nullUserEntity);
        });
    }

    @Test
    public void successGetAllUserTest(){
        List<UserEntity> listUserEntity = new ArrayList<UserEntity>();
        UserEntity userFound = new UserEntity();
        userFound.setSurname("Atom");
        listUserEntity.add(userFound);
        when(userRepository.findAll()).thenReturn(listUserEntity);
        List<UserDto> result = userService.getAllUser();
        assertEquals(result.get(0).getSurname(),listUserEntity.get(0).getSurname());
    }

    @Test
    public void failGetAllUserTest(){
        List<UserEntity> listUser = new ArrayList<UserEntity>();
        when(userRepository.findAll()).thenReturn(listUser);
        assertThrows(ResponseStatusException.class, () -> {
            userService.getAllUser();
        });
    }

    @Test
    public void successUpdateUserDataTest(){
        String userId = "userId";
        UserEntity updateUser = new UserEntity();
        updateUser.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(updateUser));
        UserDto mapToDto = mapper.toDto(updateUser);
        UserDto result = userService.updateUserData(userId,mapToDto);
        assertEquals(updateUser.getId(), result.getId());
    }

    @Test
    public void userNotFoundForUpdateUserDataTest(){
        String userId = "userId";
        UserDto updateUser =new UserDto();
        updateUser.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> {
            userService.updateUserData(userId,updateUser);
        });
    }

    @Test
    public void successUpdatePartialUserDataTest() throws IllegalAccessException {
        String userId = "userId";
        UserEntity userFound = new UserEntity();
        userFound.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(userFound));
        UserDto mapToDto = mapper.toDto(userFound);
        UserDto result = userService.updatePartialUser(userId,mapToDto);
        assertEquals(result.getId(),userFound.getId());
    }

    @Test
    public void userNotFoundForUpdatePartialUser(){
        String userId = "userId";
        UserDto nullUserDto = new UserDto();
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> {
            userService.updatePartialUser(userId,nullUserDto);
        });
    }

    @Test
    public void successDeleteUserDataTest(){
        String userId = "userId";
        UserEntity userFound = new UserEntity();
        userFound.setId(userId);
        when(userRepository.findById(userId)).thenReturn(Optional.of(userFound));
        ResponseEntity deleteSuccess = userService.deleteUser(userId);
        assertEquals(HttpStatus.NO_CONTENT, deleteSuccess.getStatusCode());
    }

    @Test
    public void userNotFoundForDeleteUserDataTest() {
        String userId = "userId";
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> {
            userService.deleteUser(userId);
        });
    }

}
