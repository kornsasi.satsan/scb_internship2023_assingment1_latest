package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Service.Producer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.KafkaException;
import org.springframework.kafka.core.KafkaTemplate;
import java.util.concurrent.CompletableFuture;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProducerServiceTest {
    @Mock
    KafkaTemplate<String, String> kafkaTemplate;
    @InjectMocks
    Producer producerService;

    @Test
    public void sendSuccessTest(){
        String message = "message for send";
        String expectedTopic = "TopicMessage";
        CompletableFuture future = new CompletableFuture();
        future.complete(null);
        when(kafkaTemplate.send(expectedTopic, message))
                .thenReturn(future);
        String result = producerService.sendMessage(message);
        assertEquals("message is sent.", result);
    }

    @Test
    public void sendFailTest(){
        String message = "message for send";
        String expectedTopic = "TopicMessage";
        when(kafkaTemplate.send(expectedTopic, message))
                .thenThrow(KafkaException.class);
        String result = producerService.sendMessage(message);
        assertEquals("message is not sent.",result);
    }

}
