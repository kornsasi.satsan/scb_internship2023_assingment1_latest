package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Controller.UserController;
import AssignSpringBoot.Assignment_SpringbootForInternship.Dto.UserDto;
import AssignSpringBoot.Assignment_SpringbootForInternship.Model.UserEntity;
import AssignSpringBoot.Assignment_SpringbootForInternship.Service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserService userService;
//    @InjectMocks
//    UserController userController;

    @Test
    public void helloTest() throws Exception{
        String url = "/api/v1/hello";
        mvc.perform(get(url)).andExpect(status().isOk());
    }

    @Test
    public void getUserTest() throws Exception{
        String url = "/api/v1/1234";
        UserDto userWasFound = new UserDto();
        userWasFound.setId("1234");
        userWasFound.setSurname("Korn");
        userWasFound.setLastname("lastName");
        userWasFound.setPhone("0811111111");
        userWasFound.setEmail("user@example.com");
        when(userService.getUserByID("1234")).thenReturn(userWasFound);
        ResultActions result = mvc.perform(get(url));
        result.andExpect(status().isOk());
    }

    @Test
    public void createUserTest() throws Exception {
        String url = "/api/v1";
        UserEntity newUser = new UserEntity();
        newUser.setId("1234");
        newUser.setSurname("Korn");
        newUser.setLastname("lastName");
        newUser.setPhone("0811111111");
        newUser.setEmail("user@example.com");
        when(userService.createNewUser(newUser)).thenReturn(newUser);
        ObjectMapper objectMapper = new ObjectMapper();
        String userJson = objectMapper.writeValueAsString(newUser);
        ResultActions result = mvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson));
        result.andExpect(status().isCreated());
    }

    @Test
    public void getAllUserTest() throws Exception{
        String url = "/api/v1";
        List<UserDto> listUserEntity = new ArrayList<UserDto>();
        UserDto userFound = new UserDto();
        userFound.setSurname("Atom");
        listUserEntity.add(userFound);
        when(userService.getAllUser()).thenReturn(listUserEntity);
        ResultActions result = mvc.perform(get(url));
        result.andExpect(status().isOk());
    }

    @Test
    public void updateUserTest() throws Exception {
        String url = "/api/v1/1234";
        UserDto updateUser = new UserDto();
        updateUser.setSurname("KornEdit");
        updateUser.setLastname("lastNameEdit");
        updateUser.setPhone("0811111111");
        updateUser.setEmail("userEdit@example.com");
        when(userService.updateUserData("1234",updateUser)).thenReturn(updateUser);
        ObjectMapper objectMapper = new ObjectMapper();
        String userJson = objectMapper.writeValueAsString(updateUser);
        ResultActions result = mvc.perform(put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson));
        result.andExpect(status().isOk());
    }

    @Test
    public void updatePartialUserTest() throws Exception{
        String url = "/api/v1/1234";
        UserDto updateUser = new UserDto();
        updateUser.setSurname("KornEdit");
        updateUser.setLastname("lastNameEdit");
        updateUser.setPhone("0811111111");
        updateUser.setEmail("userEdit@example.com");
        when(userService.updatePartialUser("1234",updateUser)).thenReturn(updateUser);
        ObjectMapper objectMapper = new ObjectMapper();
        String userJson = objectMapper.writeValueAsString(updateUser);
        ResultActions result = mvc.perform(patch(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson));
        result.andExpect(status().isOk());
    }

    @Test
    public void updatePartialUserTestException() throws Exception{
        String url = "/api/v1/1234";
        UserDto updateUser = new UserDto();
        updateUser.setId("1234");
        updateUser.setSurname("KornEdit");
        updateUser.setLastname("lastNameEdit");
        updateUser.setPhone("0811111111");
        updateUser.setEmail("userEdit@example.com");
        when(userService.updatePartialUser("1234",updateUser)).thenThrow(new IllegalAccessException());
        ObjectMapper objectMapper = new ObjectMapper();
        String userJson = objectMapper.writeValueAsString(updateUser);
        ResultActions result = mvc.perform(patch(url)
               .contentType(MediaType.APPLICATION_JSON)
               .content(userJson));
        result.andExpect(status().isForbidden());
    }

    @Test
    public void deleteUserTest() throws Exception{
        String url = "/api/v1/1234";
        ResponseEntity status = new ResponseEntity(HttpStatus.NO_CONTENT);
        when(userService.deleteUser("1234")).thenReturn(status);
        ResultActions result = mvc.perform(delete(url));
        result.andExpect(status().isNoContent());
    }

}
