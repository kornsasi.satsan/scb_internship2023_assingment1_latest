package AssignSpringBoot.Assignment_SpringbootForInternship;

import AssignSpringBoot.Assignment_SpringbootForInternship.Service.Consumer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ConsumerServiceTest {
    @Mock
    KafkaTemplate<String,String> kafkaTemplate;

    @InjectMocks
    Consumer consumerService;

    @Test
    public void successConsumerTest() throws IOException {
        String message = "message from producer";
        String result = consumerService.getMassage(message);
        assertEquals("Message is received from producer", result);
    }
}
