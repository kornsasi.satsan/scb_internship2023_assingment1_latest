CREATE SEQUENCE id_seq OWNED BY backendTeam.id;
ALTER TABLE backendTeam ALTER COLUMN id SET DEFAULT nextval('id_seq');

UPDATE
    backendTeam
SET
    id = nextval('id_seq');