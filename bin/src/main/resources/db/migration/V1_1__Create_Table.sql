-- flyway
-- ---
-- version: 1
-- description: Create table user
-- flyway

CREATE TABLE backendTeam (
    id INTEGER PRIMARY KEY NOT NULL,
    team VARCHAR(50) ,
    surname VARCHAR(50) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    nickname VARCHAR(10),
    teamPosition VARCHAR(50),
    email VARCHAR(50) NOT NULL,
    phone VARCHAR(10) NOT NULL
);

